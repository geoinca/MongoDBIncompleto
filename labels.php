<?php
// This script is included in the showXYZ.php scripts.
// In those scripts $document has been initialized with the current post

echo '<h4>Etiquetas</h4>';	

// Show the tags of the current post

/**********
** YOUR CODE HERE:
Iterate through the post (use the variable $document) and get the tags
For each tag, print an hyperlink with the tag text
The link points to index.php with two parameters:
* command =  showPostsByTag
* tag = the tag
**********/
echo "<div class=\"tagcloud\">";
foreach($document['tags'] as $tag){
    echo "<span class=\"tagbox\"><a href=index.php?command=showPostsByTag&tag=$tag class=\"taglink\">$tag</a></span>  ";
}
echo "</div>";	

?>
