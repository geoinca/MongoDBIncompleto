<?php
require 'vendor/autoload.php';

// Setup MongoDB connection
$connection = NULL;

/**********
** YOUR CODE HERE:
Assign to $connection the connection to MongoDB
**********/

// Select the "posts" collection in the database "blog"
$collection = NULL;
$connection = new MongoDB\Client('mongodb://localhost:27017');
/**********
** YOUR CODE HERE:
Assign to $collection the the "posts" collection of the database "blog"
**********/
$collection = $connection->blog->posts;
//var_dump($collection);
?>
