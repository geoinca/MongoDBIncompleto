<?php
// This script is included in the showXYZ.php scripts.
// In those scripts $document has been initialized with the current post
$totalComentarios=$document['comments']->count();
$id = $document['_id'];
	echo '<div class="blog-post">';
	echo '<div class="spacetop25 spacebottom25">' ;
	// Show post title
	echo '<h2 class="text-uppercase text-secondary mb-0">';
	/**********
	** YOUR CODE HERE:
	Print the title of the post (use the variable $document)
	**********/
	$title = $document['title'];
	echo $title;

	echo '</h2>';
	
	// Show post date and author
	echo '<p class="blog-post-meta">';

	/**********
	** YOUR CODE HERE:
	Print the date of the post (use the variable $document)
	Format the date with this PHP method: toDateTime()->format('l, d F Y' )
	**********/
	$date = $document['date'];
	echo "<i class=\"far fa-calendar-alt\"></i> ";
	echo $date->toDateTime()->format('l, d F Y');
	echo " <i class=\"fas fa-user\"></i>  by <a href=\"#\">";
	/**********
	** YOUR CODE HERE:
	Print the author of the post (use the variable $document)
	Note that it is a hyperlink that does not point to anything. Do not change it.
	**********/
	/*******Cod
	 * 	$id = $document['_id'];
	echo '<a href = index.php?command=showMore&id=' . $id . '> Mostrar más</a>';
	echo "<li><a href='index.php?command=showPostsCommentedByAuthor&author=$author'>$author</a></li>";
	<i class="fas fa-comment"></i> 
	*/
	$author = $document['author'];
	echo "  ";
	echo $author;
	echo "</a>  <a href=\"index.php?command=showAllCommentedPost&id=$id\"><i class=\"fas fa-comment\"></i> $totalComentarios </a></p>";
	echo '</div>';
?>
